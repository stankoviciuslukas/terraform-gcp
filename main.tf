module "gcp_instance_porfolio" {
  source              = "./modules/gcp-instance"
  instance_name       = "portfolio"
  instance_type       = "f1-micro"
  instance_zone       = "us-central1-a"
  instance_image      = "ubuntu-os-cloud/ubuntu-2004-lts"
  vpc_name            = "portfolio-vpc"
  allowed_ports       = ["22", "80", "443"]
  subnet_name         = "portoflio-subnet"
  subnet_ipaddr_range = "10.20.0.0/16"
  subnet_region       = "us-central1"
}

module "gcp_instance_smtp" {
  source              = "./modules/gcp-instance"
  instance_name       = "smtp"
  instance_type       = "f1-micro"
  instance_zone       = "us-central1-a"
  instance_image      = "ubuntu-os-cloud/ubuntu-2004-lts"
  vpc_name            = "smtp-vpc"
  allowed_ports       = ["22", "25", "587"]
  subnet_name         = "smtp-subnet"
  subnet_ipaddr_range = "10.20.0.0/16"
  subnet_region       = "us-central1"
}
