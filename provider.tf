provider "google" {
  project     = "stankovicius-lt"
  region      = "us-central1"
  zone        = "us-central1-c"
}

terraform {
  backend "gcs" {
    bucket  = "terraform-state-stankovicius-lt"
    prefix  = "terraform/state"
  }
}
