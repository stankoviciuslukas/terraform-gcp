# terraform-gcp

Terraform configuration files for provisioning instances on Google Cloud.

## Requirements

- Service Account credentials
- Manually configured storage
