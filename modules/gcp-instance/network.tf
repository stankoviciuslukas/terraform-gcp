resource "google_compute_network" "vpc" {
  name                    = var.vpc_name
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "subnet" {
  name                    = var.subnet_name
  ip_cidr_range           = var.subnet_ipaddr_range
  network                 = google_compute_network.vpc.id
  region                  = var.subnet_region
}

resource "google_compute_firewall" "firewall" {
  name    = "${var.vpc_name}-firewall"
  network = google_compute_network.vpc.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = var.allowed_ports
  }

  source_ranges = ["0.0.0.0/0"]
}
