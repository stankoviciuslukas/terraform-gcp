variable "instance_name" {
  description = "The name to use for intance resource"
  type        = string
}

variable "instance_type" {
  description = "The name of intance type in Google Cloud resources"
  type        = string
}

variable "instance_zone" {
  description = "The name of zone to deploy intance to"
  type        = string
}

variable "instance_image" {
  description = "The name of image to use for intance deployment"
  type        = string
}

variable "vpc_name" {
  description = "The name of VPC network"
  type        = string
}

variable "subnet_name" {
  description = "The name of subnet to be used by instances"
  type        = string
}

variable "subnet_ipaddr_range" {
  type        = string
}

variable "subnet_region" {
  type        = string
}

variable "allowed_ports" {
  type        = list
}
